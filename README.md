# CJxml
[![Release](https://jitpack.io/v/sijanmaharjan/CJxml.svg)](https://jitpack.io/#sijanmaharjan/CJxml)
[Javadoc](https://jitpack.io/com/github/sijanmaharjan/CJxml/5.1/javadoc/cj/xml/package-summary.html)
<br>
This is a java XML library. It uses [jdom2](https://mvnrepository.com/artifact/org.jdom/jdom2/2.0.6) and [jaxen](https://mvnrepository.com/artifact/jaxen/jaxen/1.1.6) libraries
<br>
#### [What's New?](https://github.com/sijanmaharjan/CJxml/releases/tag/5.0)
Check [this test file](https://github.com/sijanmaharjan/CJxml/blob/master/src/test/java/cj/xml/XMLTest.java) for more usage examples.
### Include CJxml in your project
  * To use CJxml with Java projects
  
#### Using Gradle 
* Add repository
```
repositories {
    ...
    maven { url 'https://www.jitpack.io' }
}
```
* Add dependency
```
dependencies {
    compile 'com.github.sijanmaharjan:CJxml:5.1'
}
```

### Code Examples
[Check example creating hibernate configuration file](https://github.com/sijanmaharjan/CJxml/blob/master/examples/creating%20hibernate%20configuration%20file.md#creating-hibernate-configuration-file)
#### latest features with functional interfaces (using lambda)
#### 1. writing xml with lambda
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/7.png)
  
##### Output
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/8.png)
    
#### 2. Multiple tags of same blueprint with text-contents with lambda
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/9.png)
    
##### Output
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/10.png)
    
#### 3. Multiple tags of same blueprint having inner-tags with lambda
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/11.png)

##### Output
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/12.png)
    
#### 4. Multiple tags of same blueprint having combo-contents(mixed) with lambda
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/13.png)

##### Output
![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/14.png)
    
  #### Pre-existing methods
  #### 1. Creating xml file and writing one tag
  
  ![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/1.png)
  
  ##### Output
  
  ![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/2.png)
  
  #### 2. Multiple tags
  
  ![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/3.png)
  
  ##### Output
  
  ![alt text](https://github.com/sijanmaharjan/laughing-chainsaw/blob/master/CJxml/images/4.png)
  
CJxml is released under the [GNU LESSER GENERAL PUBLIC LICENSE](LICENSE).

```
Copyright (C) 2017 Sijan Maharjan
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.
```

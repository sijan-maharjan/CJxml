package cj.xml;

import cj.xml.platform.ContentType;
import org.jdom2.Element;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class XMLTest {
    List<String> cars = Arrays.asList("Lamborghini", "Ferrari", "Ford", "Bugatti", "Mercedes");
    List<String> models = Arrays.asList("Aventador", "abc", "Mustang", "def", "ghi");

    @Test
    public void testCreatingXMLWithLambda() {
        XML xml = new XML(new File("test.xml"));
        xml.write(Tag.create("cars").addAll(() -> Arrays.asList(
                Tag.create("car", XMLAttribute.create("name", "Lamborghini"))
                        .addAll(() -> Arrays.asList(
                                Tag.create("model", "Siesto-Elemento")
                        )),
                Tag.create("car", XMLAttribute.create("name", "Ferrari")),
                Tag.create("car", XMLAttribute.create("name", "Bugatti"))
        )));


        XML xml2 = new XML(new File("test.xml"));
        Element el = xml2.find("car", "name", "Lamborghini");
        List<Element> childs = el.getChildren();

        Assert.assertEquals(childs.size(), 1);
        Assert.assertEquals(childs.get(0).getText(), "Siesto-Elemento");

    }

    @Test
    public void testAddingMultipleTagsWithTextContentWithLambda() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleTextTags(5,"car",
                        (n,i)-> Arrays.asList(XMLAttribute.create("id", String.valueOf(i+1))),
                        (n,i)->cars.get(i),
                        (n,i)->"Car ID: "+(i+1))
        );

        XML xml2 = new XML(new File("test.xml"));
        Element el = xml2.find("car", "id", "1");
        List<Element> childs = el.getChildren();

        Assert.assertEquals(0, childs.size());
        Assert.assertEquals(el.getText(), cars.get(0));

    }

    @Test
    public void testAddingMultipleTagsHavingInnerTagsWithLambda() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleTagsWithInnerTags(5, "car",
                        (n, i) -> Arrays.asList(XMLAttribute.create("name", cars.get(i))),
                        (n, i) -> ()->Arrays.asList( Tag.create("model", models.get(i))),
                        (n, i) -> "Car ID: "+(i+1))
        );

        XML xml2 = new XML(new File("test.xml"));
        Element el = xml2.find("car", "name", cars.get(3));
        List<Element> childs = el.getChildren();

        Assert.assertEquals(1, childs.size());
        Assert.assertEquals(childs.get(0).getText(), models.get(3));

    }

    @Test
    public void testAddingMultipleTagsWithComboContentWithLambda() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleComboTags(5, "car",
                        (n, i) -> (i == 0 || i == 2)? ContentType.tag : ContentType.text,
                        (n, i) -> Arrays.asList(XMLAttribute.create("name", cars.get(i))),
                        (n, i) -> "",
                        (n, i) -> ()->Arrays.asList( Tag.create("model", models.get(i))),
                        (n, i) -> "Car ID: "+(i+1))
        );

        XML xml2 = new XML(new File("test.xml"));
        Element el = xml2.find("car", "name", cars.get(2));
        List<Element> childs = el.getChildren();

        Assert.assertEquals(1, childs.size());
        Assert.assertEquals(models.get(2), childs.get(0).getText());

    }

    @Test
    public void testListing() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleTextTags(5,"car",
                        (n,i)-> Arrays.asList(XMLAttribute.create("id", String.valueOf(i+1))),
                        (n,i)->cars.get(i),
                        (n,i)->"Car ID: "+(i+1))
        );

        List<Element> list1 = xml.list("car");
        Assert.assertEquals(cars.size(), list1.size());

        List<Element> list2 = xml.list("car", "id", "6");
        Assert.assertEquals(0, list2.size());

        List<Element> list3 = xml.list("car", XMLAttribute.create("id", "3"));
        Assert.assertEquals(1, list3.size());

        List<Element> list4 = xml.list("car", "Ford", XMLAttribute.create("id", "3"));
        Assert.assertEquals(1, list4.size());

        List<Element> list5 = xml.list("car", "Ford");
        Assert.assertEquals(1, list5.size());
    }

    @Test
    public void testFinding() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleTextTags(5,"car",
                        (n,i)-> Arrays.asList(XMLAttribute.create("id", String.valueOf(i+1))),
                        (n,i)->cars.get(i),
                        (n,i)->"Car ID: "+(i+1))
        );

        Element el1 = xml.find("car");
        Assert.assertEquals(el1.getText(), cars.get(0));

        Element el2 = xml.find("car", "id", "2");
        Assert.assertEquals(el2.getText(), cars.get(1));

        Element el3 = xml.find("car", XMLAttribute.create("id", "5"));
        Assert.assertEquals(el3.getText(), cars.get(4));

        Element el4 = xml.find("car", "Ford", XMLAttribute.create("id", "2"));
        Assert.assertNull(el4);

        Element el5 = xml.find("car", "Ford");
        Assert.assertNotNull(el5);
    }

    @Test
    public void testTagScopeQuerying() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleComboTags(5, "car",
                        (n, i) -> (i == 0 || i == 2)? ContentType.tag : ContentType.text,
                        (n, i) -> Arrays.asList(XMLAttribute.create("name", cars.get(i))),
                        (n, i) -> "",
                        (n, i) -> ()->Arrays.asList( Tag.create("model", models.get(i))),
                        (n, i) -> "Car ID: "+(i+1))
        );

        Tag tag = Tag.parseTag(xml.find("car"));
        List<Element> elements = tag.list("model");

        Assert.assertNull(tag.find("model", "type", "super"));

        Assert.assertEquals(1, elements.size());
        Assert.assertEquals(models.get(0), elements.get(0).getText());
    }

    @Test
    public void testTagSearching() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleComboTags(5, "car",
                        (n, i) -> (i == 0 || i == 2)? ContentType.tag : ContentType.text,
                        (n, i) -> Arrays.asList(XMLAttribute.create("name", cars.get(i))),
                        (n, i) -> "",
                        (n, i) -> ()->Arrays.asList( Tag.create("model", models.get(i))),
                        (n, i) -> "Car ID: "+(i+1))
        );

        Tag tag = xml.get("car#1 model");
        Assert.assertNull(tag);

        Tag tag2 = xml.get("car[name=Ford] model");
        Assert.assertNotNull(tag2);

        Assert.assertEquals(models.get(2), tag2.getText());
    }

    @Test
    public void testTagSmartSearching() {
        XML xml = new XML(new File("test.xml"));
        xml.write(
                Tag.create("cars").addMultipleComboTags(5, "car",
                        (n, i) -> (i == 0 || i == 2)? ContentType.tag : ContentType.text,
                        (n, i) -> Arrays.asList(XMLAttribute.create("name", cars.get(i))),
                        (n, i) -> "",
                        (n, i) -> ()->Arrays.asList( Tag.create("model", models.get(i))),
                        (n, i) -> "Car ID: "+(i+1))
        );

        Tag tag = xml.get(tag1->
                tag1.find(c->
                        c.getName().equals("car") && c.getAttribute("name").getValue().equals("Ford")
                ).find(s->
                        s.getName().equals("model")
                )
        );
        Assert.assertNotNull(tag);

        Assert.assertEquals(models.get(2), tag.getText());
    }

    @Test
    public void testFiltering(){
        XML xml = new XML(new File("test.xml"));
        xml.write(Tag.create("students").addAll(() -> Arrays.asList(
                Tag.create("student", XMLAttribute.create("name", "Sijan Maharjan"))
                        .addAll(() -> Arrays.asList(
                                Tag.create("roll", "12")
                        )),
                Tag.create("student", XMLAttribute.create("name", "Ishan Maharjan"))
                        .addAll(() -> Arrays.asList(
                                Tag.create("roll", "20")
                        )),
                Tag.create("count", XMLAttribute.create("status", "There are 3 students in class 1")),
                Tag.create("attendance", XMLAttribute.create("status", "There are 2/3 students in class 1"))
        )));

        List<Element> students = xml.filter("//student[roll<20]");
        Assert.assertEquals(1, students.size());

        students = xml.filter("//student[roll mod 2 = 0]");
        Assert.assertEquals(2, students.size());

        students = xml.filter("//*[boolean(number(substring-before(substring-after(@status, \"There are \"), \" students in class 1\")))]");
        Assert.assertEquals(1, students.size());
    }
}

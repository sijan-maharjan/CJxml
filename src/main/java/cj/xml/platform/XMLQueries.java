/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * rootElement program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * rootElement program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with rootElement program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml.platform;

import cj.xml.Tag;
import cj.xml.XMLAttribute;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.function.Predicate;

/**
 * XML Queries over an element
 * @since 4.0
 */
public interface XMLQueries {

    File getFile();
    Element getRoot();
    
    /**
     * find element with tagname, attribute and value
     * @param name tagname
     * @param attribute attribute name
     * @param value attribute value
     * @return element
     */
    default Element find(String name, String attribute, String value){
        try {
            Stack<Element> stack = new Stack<>();
            stack.push(getRoot());
            while (!stack.isEmpty()) {
                Element root = stack.pop();
                if (root.getName().equals(name) && root.getAttribute(attribute).getValue().equals(value)) {
                    return root;
                }
                for (Element element : root.getChildren()) {
                    try {
                        if (element.getName().equals(name) && element.getAttribute(attribute).getValue().equals(value)) {
                            return element;
                        } else {
                            stack.push(element);
                        }
                    } catch (Exception e) {
                        stack.push(element);
                    }
                }
            }
            return null;
        }catch(NullPointerException ne){
            return null;
        }
    }

    /**
     * find element with tagname, and multiple attributes
     * 
     * @param name tagname
     * @param attributes attributes
     * @return element
     */
    default Element find(String name, XMLAttribute... attributes){
        if(attributes.length ==0) return find(name);
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && contains(root, attributes)) {
                return root;
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && contains(element, attributes)) {
                        return element;
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return null;
    }

    /**
     * find element with tagname, and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @return element
     */
    default Element find(String name, String textContent){
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && root.getText().equals(textContent)) {
                return root;
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && element.getText().equals(textContent)) {
                        return element;
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return null;
    }

    /**
     * find element with tagname, attribute and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @param attributes attributes
     * @return element
     */
    default Element find(String name, String textContent, XMLAttribute ... attributes){
        if(attributes.length ==0) return find(name, textContent);
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && root.getText().equals(textContent) && contains(root, attributes)) {
                return root;
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && element.getText().equals(textContent) && contains(element, attributes)) {
                        return element;
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return null;
    }

    /**
     * find element with tagname
     * 
     * @param name tagname
     * @return element
     */
    default Element find(String name){
        Element rootElement = getRoot();
        Stack<Element> stack = new Stack<>();
        if(rootElement.getName().equals(name)){
            return rootElement;
        }else {
            stack.push(rootElement);
            while (!stack.isEmpty()) {
                Element root = stack.pop();
                for (Element element : root.getChildren()) {
                    if (element.getName().equals(name)) {
                        return element;
                    } else {
                        stack.push(element);
                    }
                }
            }
            return null;
        }
    }

    /**
     * lists element with tagname, attribute and value
     * 
     * @param name tagname
     * @param attribute attribute name
     * @param value attribute value
     * @return list
     */
    default List<Element> list(String name, String attribute, String value){
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && root.getAttribute(attribute).getValue().equals(value)) {
                elements.add(root);
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && element.getAttribute(attribute).getValue().equals(value)) {
                        elements.add(element);
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return elements;
    }

    /**
     * lists element with tagname, and multiple attributes
     * 
     * @param name tagname
     * @param attributes attributes
     * @return list
     */
    default List<Element> list(String name, XMLAttribute... attributes){
        if(attributes.length ==0) return list(name);
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && contains(root, attributes)){
                elements.add(root);
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && contains(element, attributes)) {
                        elements.add(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return elements;
    }

    /**
     * lists element with tagname, and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @return list
     */
    default List<Element> list(String name, String textContent){
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && root.getText().equals(textContent)) {
                elements.add(root);
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && element.getText().equals(textContent)) {
                        elements.add(element);
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return elements;
    }

    /**
     * lists element with tagname, attribute and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @param attributes attributes
     * @return list
     */
    default List<Element> list(String name, String textContent, XMLAttribute ... attributes){
        if(attributes.length ==0) return list(name, textContent);
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (root.getName().equals(name) && root.getText().equals(textContent) && contains(root, attributes)) {
                elements.add(root);
            }
            for(Element element : root.getChildren()){
                try {
                    if (element.getName().equals(name) && element.getText().equals(textContent) && contains(element, attributes)) {
                        elements.add(element);
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return elements;
    }

    /**
     * lists element with tagname
     * 
     * @param name tagname
     * @return list
     */
    default List<Element> list(String name){
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()) {
            Element root = stack.pop();
            if(root.getName().equals(name)){
                elements.add(root);
            }
            for (Element element : root.getChildren()) {
                if (element.getName().equals(name)) {
                    elements.add(element);
                } else {
                    stack.push(element);
                }
            }
        }
        return elements;
    }

    /**
     * first element with tagname, attribute and value
     * 
     * @param name tagname
     * @param attribute attribute name
     * @param value attribute value
     * @return element
     */
    default Element first(String name, String attribute, String value){
        return find(name, attribute, value);
    }

    /**
     * first element with tagname, and multiple attributes
     * 
     * @param name tagname
     * @param attributes attributes
     * @return element
     */
    default Element first(String name, XMLAttribute... attributes){
        return find(name, attributes);
    }

    /**
     * first element with tagname, and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @return element
     */
    default Element first(String name, String textContent){
        return find(name, textContent);
    }

    /**
     * first element with tagname, attribute and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @param attributes attributes
     * @return element
     */
    default Element first(String name, String textContent, XMLAttribute ... attributes){
        return find(name, textContent, attributes);
    }

    /**
     * first element with tagname
     * 
     * @param name tagname
     * @return element
     */
    default Element first(String name){
        return find(name);
    }

    /**
     * last element with tagname, attribute and value
     * 
     * @param name tagname
     * @param attribute attribute name
     * @param value attribute value
     * @return element
     */
    default Element last(String name, String attribute, String value){
        List<Element> list = list(name, attribute, value);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * last element with tagname, and multiple attributes
     * 
     * @param name tagname
     * @param attributes attributes
     * @return element
     */
    default Element last(String name, XMLAttribute... attributes){
        List<Element> list = list(name, attributes);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * last element with tagname, and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @return element
     */
    default Element last(String name, String textContent){
        List<Element> list = list(name, textContent);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * last element with tagname, attribute and its text content
     * 
     * @param name tagname
     * @param textContent text content
     * @param attributes attributes
     * @return element
     */
    default Element last(String name, String textContent, XMLAttribute ... attributes){
        List<Element> list = list(name, textContent, attributes);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * last element with tagname
     * 
     * @param name tagname
     * @return element
     */
    default Element last(String name){
        List<Element> list = list(name);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * checks if element contains all specified attributes
     * @param element
     * @param attributes
     * @return
     */
    default boolean contains(Element element, XMLAttribute ... attributes){
        boolean include = true;
        for(Attribute attr: attributes){
            if(include) {
                Attribute attribute = element.getAttribute(attr.getName());
                if(attribute != null) {
                    String v1 = attribute.getValue();
                    String v2 = attr.getValue();
                    include = v1.equals(v2);
                }else{
                    include = false;
                }
            }
        }
        return include;
    }

    /**
     * search tag traditional way
     * @param tag
     * @return
     */
    default Tag get(String tag){
        try {
            Tag root = Tag.parseTag(getRoot());
            for (String cmd : tag.split(" ")) {
                if (cmd.contains("#")) {
                    String[] cmds = cmd.split("#");
                    String tagName = cmds[0].trim();
                    String id = cmds[1].trim();
                    root = Tag.parseTag(root.find(tagName, "id", id));
                } else if (cmd.contains("[")) {
                    String[] cmds = cmd.split("\\[");
                    String tagName = cmds[0].trim();
                    String attr = cmds[1].replace("]", "");
                    String[] attrs = attr.split(",");
                    List<XMLAttribute> attributeList = new ArrayList<>();
                    for (String at : attrs) {
                        String nv[] = at.split("=");
                        String attrName = nv[0].trim();
                        String attrVal = nv[1].trim();
                        attributeList.add(XMLAttribute.create(attrName, attrVal));
                    }
                    root = Tag.parseTag(root.find(tagName, attributeList.toArray(new XMLAttribute[attributeList.size()])));
                } else if (cmd.contains("=")) {
                    String[] cmds = cmd.split("=");
                    String tagName = cmds[0].trim();
                    String text = cmds[1].trim();
                    root = Tag.parseTag(root.find(tagName, text));
                } else {
                    root = Tag.parseTag(root.find(cmd.trim()));
                }
            }
            return root;
        }catch (NullPointerException ne){
            return null;
        }
    }

    /**
     * search tag smartly with lambda
     * @param smartTag
     * @return
     */
    default Tag get(SmartTag smartTag){
        return smartTag.getTag(Tag.parseTag(getRoot()));
    }


    /**
     * find element with lambda
     * 
     * @param predicate predicate
     * @return element
     */
    default Tag find(Predicate<? super Element> predicate){
        try {
            Stack<Element> stack = new Stack<>();
            stack.push(getRoot());
            while (!stack.isEmpty()) {
                Element root = stack.pop();
                if (predicate.test(root)) {
                    return Tag.parseTag(root);
                }
                for (Element element : root.getChildren()) {
                    try {
                        if (predicate.test(element)) {
                            return Tag.parseTag(element);
                        } else {
                            stack.push(element);
                        }
                    } catch (Exception e) {
                        stack.push(element);
                    }
                }
            }
            return null;
        }catch(NullPointerException ne){
            return null;
        }
    }

    /**
     * lists element with lambda
     * 
     * @param predicate predicate
     * @return list
     */
    default List<Tag> list(Predicate<? super Element> predicate){
        List<Element> elements = new ArrayList<>();
        Stack<Element> stack = new Stack<>();
        stack.push(getRoot());
        while (!stack.isEmpty()){
            Element root = stack.pop();
            if (predicate.test(root)) {
                elements.add(root);
            }
            for(Element element : root.getChildren()){
                try {
                    if (predicate.test(element)) {
                        elements.add(element);
                    } else {
                        stack.push(element);
                    }
                }catch (Exception e){
                    stack.push(element);
                }
            }
        }
        return Tag.parseTag(elements);
    }

    /**
     * first element with lambda
     * 
     * @param predicate predicate
     * @return element
     */
    default Tag first(Element rootElement,Predicate<? super Element> predicate){
        return find(predicate);
    }

    /**
     * last element with lambda
     * 
     * @param predicate predicate
     * @return element
     */
    default Tag last(Predicate<? super Element> predicate){
        List<Tag> list = list(predicate);
        if(list.isEmpty()) return null;
        return list.get(list.size()-1);
    }

    /**
     * last element with XPath filter
     * @param xPathExp <a href="https://www.tutorialspoint.com/xpath/xpath_expression.htm">XPath 1.0</a> expressions
     * @return filtered elements
     */
    default List<Element> filter(String xPathExp){
        try {
            SAXBuilder jdomBuilder = new SAXBuilder();
            Document jdomDocument = jdomBuilder.build(getFile());

            XPathFactory xFactory = XPathFactory.instance();
            XPathExpression<Element> expr = xFactory.compile(xPathExp, Filters.element());
            return expr.evaluate(jdomDocument);
        }catch (IOException | JDOMException e) {
            return Collections.emptyList();
        }
    }
}

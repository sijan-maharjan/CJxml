/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml;
import cj.xml.platform.*;
import org.jdom2.*;

import java.io.File;
import java.util.*;

/**
 * XML Tag or Element
 * @since 1.0
 */
public abstract class Tag extends Element implements XMLQueries {

    private Tag(){}

    /**
     * initialize with name
     * @param name tagname
     */
    private Tag(String name){
        super(name);
    }

    /**
     * initialize with name and namespace
     * @param name tagname
     * @param ns namespace
     */
    private Tag(String name, Namespace ns){
        super(name, ns);
    }

    /**
     * initialize with name and text content
     * @param name tagname
     * @param textContent  textcontent
     */
    private Tag(String name, String textContent) {
        super(name);
        this.setText(textContent);
    }

    /**
     * initialize with name and attributes
     * @param name tagname
     * @param attributes  attributes
     */
    private Tag(String name, XMLAttribute ... attributes) {
        this(name);
        List<XMLAttribute> attributeList = Arrays.asList(attributes);
        this.setAttributes(attributeList);
    }

    /**
     * initialize with name and attributes
     * @param name tagname
     * @param attributes  attributes
     */
    private Tag(String name, List<XMLAttribute> attributes) {
        this(name);
        this.setAttributes(attributes);
    }

    /**
     * initialize with name, text content and attributes
     * @param name tagname
     * @param textContent  textcontent
     * @param attributes  attributes
     */
    private Tag(String name, String textContent, XMLAttribute ... attributes) {
        this(name, textContent);
        List<XMLAttribute> attributeList = Arrays.asList(attributes);
        this.setAttributes(attributeList);
    }

    public static Tag create(String name) {
        return new Tag(name) {};
    }

    public static Tag create(String name, Namespace ns) {
        return new Tag(name, ns) {};
    }

    public static Tag create(String name, String textContent) {
        return new Tag(name, textContent) {};
    }

    public static Tag create(String name, XMLAttribute ... attributes) {
        return new Tag(name, attributes) {};
    }

    public static Tag create(String name, List<XMLAttribute> attributes) {
        return new Tag(name, attributes) {};
    }

    public static Tag create(String name, String textContent, XMLAttribute ... attributes) {
        return new Tag(name, textContent, attributes) {};
    }

    @Override
    public Element getRoot() {
        return this;
    }

    @Override
    public File getFile() {
        throw new UnsupportedOperationException("\nFilter with cj.xml.Tag is not Supported Yet!\nPlease use cj.xml.XML object to filter elements!");
    }

    public static Tag parseTag(Element element){
        List<XMLAttribute> attrs = new ArrayList<>();
        try {
            for (Attribute attribute : element.getAttributes()) {
                attrs.add(XMLAttribute.create(attribute.getName(), attribute.getValue()));
            }
        }catch (NullPointerException ne){}
        Tag tag = Tag.create(element.getName(), attrs);
        tag.setContent(element.cloneContent());
        return tag;
    }

    public static List<Tag> parseTag(List<Element> elements){
        List<Tag> tags = new ArrayList<>();
        for(Element element : elements){
            tags.add(Tag.parseTag(element));
        }
        return tags;
    }

    /**
     * invoke to add attributes
     * @param attributes  attributes
     * @return Tag rootTag
     */
    final public Tag add(XMLAttribute ... attributes){
        List<XMLAttribute> attributeList = Arrays.asList(attributes);
        this.setAttributes(attributeList);
        return this;
    }

    /**
     * invoke to add child tag
     * @param tag tag
     * @return Tag rootTag
     */
    final public Tag add(Tag tag){
        this.addContent(tag);
        return this;
    }

    /**
     * invoke to add number of child tags
     * @param tags tags
     * @return Tag rootTag
     */
    final public Tag add(Tag ... tags){
        this.add(Arrays.asList(tags));
        return this;
    }


    /**
     * invoke to add number of child tags
     * @param tags tags
     * @return Tag rootTag
     */
    final public Tag add(List<Tag> tags){
        this.addContent(tags);
        return this;
    }

    /**
     * invoke to add tags provided through {@link Tags} functional interface
     * @param tags tags
     * @return Tag rootTag
     */
    final public Tag addAll(Tags tags){
        this.addContent(tags.getAll());
        return this;
    }

    /**
     * invoke to add multiple child tags
     * @param total times to repeat
     * @param name tagName
     * @param attributes attributes
     * @param textContents contents
     * @param comments comments
     * @return Tag rootTag
     */
    final public Tag addMultipleTextTags(int total, String name, Attributes attributes, TextContents textContents, Comments comments){
        return this.addMultipleComboTags(total, name, (n,i)->ContentType.text, attributes, textContents, null, comments);
    }

    /**
     * invoke to add multiple child tags
     * @param total times to repeat
     * @param name tagName
     * @param attributes attributes
     * @param comments comments
     * @param innerTags contents
     * @return Tag rootTag
     */
    final public Tag addMultipleTagsWithInnerTags(int total, String name, Attributes attributes, InnerTags innerTags, Comments comments){
        return this.addMultipleComboTags(total, name, (n,i)->ContentType.tag, attributes, null, innerTags, comments);
    }

    /**
     * invoke to add multiple child tags
     * @param total times to repeat
     * @param name tagName
     * @param types types
     * @param attributes attributes
     * @param textContents contents
     * @param innerTags innerTags
     * @param comments comments
     * @return Tag rootTag
     */
    final public Tag addMultipleComboTags(int total, String name, Types types, Attributes attributes, TextContents textContents, InnerTags innerTags, Comments comments){
        List<Content> contents = new ArrayList<>();
        for(int i=0; i < total; i++){
            //adding comment for tag
            if(comments != null) {
                String comment = comments.forTag(name, i);
                if (comment != null) {
                    contents.add(new Comment(comment));
                }
            }

            //creating tag
            Tag tag;
            if(attributes != null) {
                List<XMLAttribute> xmlAttributes = attributes.forTag(name, i);
                if(xmlAttributes != null && !xmlAttributes.isEmpty()) {
                    tag = Tag.create(name, xmlAttributes);
                }else{
                    tag = Tag.create(name);
                }
            }else{
                tag = Tag.create(name);
            }

            //adding content to tag
            ContentType type = types.forTag(name, i);

            if(type == ContentType.text) {
                if (textContents != null) {
                    String textContent = textContents.forTag(name, i);
                    if (textContent != null) {
                        tag.setText(textContent);
                    }
                }
            } else { // if type == ContentType.tag
                if(innerTags != null) {
                    Tags tags = innerTags.forTag(name, i);
                    if(tags != null) {
                        tag.addAll(tags);
                    }
                }
            }
            contents.add(tag);
        }

        this.addContent(contents);
        return this;
    }

    /**
     * invoke to add comment
     * @param comment comment
     * @return Tag rootTag
     */
    final public Tag addComment(String comment){
        this.addContent(new Comment(comment));
        return this;
    }


}

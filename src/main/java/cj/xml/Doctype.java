/*
 * Copyright (C) 2017 Sijan Maharjan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cj.xml;

import org.jdom2.DocType;

/**
 * XML DOCTYPE
 * @since 1.0
 */
public class Doctype extends DocType {
    /**
     * initialize with element name
     * @param elementName element name
     */
    public Doctype(String elementName){
        super(elementName);
    }

    /**
     * initialize with element name, systemID
     * @param elementName element name
     * @param systemID system id
     */
    public Doctype(String elementName, String systemID){
        super(elementName, systemID);
    }

    /**
     * initialize with element name, publicID, systemID
     * @param elementName element name
     * @param publicID public id
     * @param systemID system id
     */
    public Doctype(String elementName, String publicID, String systemID){
        super(elementName, publicID, systemID);
    }
}
